#include <stdio.h>
#include <locale.h>


unsigned long hcf(unsigned long a, unsigned long b) {
	if(b==0) {
		return a;
	} else {
		return hcf(b, a%b);
	}
}

unsigned long fme(unsigned long g, unsigned long x, unsigned long p) {
	unsigned long d=g;
	unsigned long e=x;
	unsigned long s=1;

	while(e>0) {
		if(e%2==1) {
			s=(s*d)%p;
		}
		d=(d*d)%p;
		e=e/2;
	}

	return s;

} 


// ���������� ��������
unsigned long dl(unsigned long y, unsigned long g, unsigned long p) {
	unsigned long x=1;
	unsigned long modVal=0;
	while(x<p) {
		modVal=fme(g, x, p);
		if(modVal==y) {
			return x;
		} 
		x=x+1;
	}

	return 0;

}


// ��������� �������� ������

unsigned long imp(unsigned long y, unsigned long p) {
	unsigned long x=1;
	unsigned long modVal=0;
	while(x<p) {
		modVal=(x*y)%p;
		if(modVal==1) {
			return x;
		} 
		x=x+1;
	}

	return 0;
}

int main(int argc, char **argv) {

	unsigned long int a, b, g, x, p, y, m, k, s, sInv;
	char choice;
	p=65537;
	g=3;

	setlocale(LC_ALL,"Russian");
	printf("P %lu\n", p);
	printf("���������� ����� %lu � %lu\n", p, g);

	x = 123;
	printf("��������� ����: %lu\n", x);
	y = fme(g, x, p);
	printf("�������� ����: %lu\n", y);

	//����������� ��� ���������
	m = 12345;

	//���������� ��������� ������ ����� p ���.
	k=rand()%(p-1)+1;
	//��������� ���������� ���������� �������
	a=fme(g, k, p);
	b=(m*fme(y, k, p))%p;
	printf("���������� ������: (%lu, %lu)\n", a, b);

	s=fme(a, x, p);
	sInv=imp(s, p);
	m=(sInv*b)%p;
	printf("������������� ������: %lu\n", m);

	return 0;
}